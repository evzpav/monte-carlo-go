FROM golang:1.18 as base
WORKDIR /go/monte-carlo-go
COPY . .
RUN GOOS=js GOARCH=wasm go build -v -o ./go/monte-carlo-go/app/main.wasm ./cmd/webassembly/main.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o /go/bin/monte-carlo-server ./cmd/server/main.go


FROM alpine:3.16 as image
COPY --from=base /go/bin/monte-carlo-server /monte-carlo-server
COPY --from=base /go/monte-carlo-go/app  /app
ENTRYPOINT [ "./monte-carlo-server" ]
NAME    = $(shell basename $(CURDIR))
BUILD   = $(shell git rev-parse --short HEAD)
IMAGE   = $(NAME):$(BUILD)

target: 
	DOCKER_BUILDKIT=1 \
	docker build --progress=plain \
		--tag $(IMAGE) \
		--target=$(TARGET) \
		--file= .


image: 
	make target TARGET=image

run-docker: image
	docker run --rm \
		--name $(NAME) \
		--network=host \
		-e HOST=localhost \
		-e PORT=8888 \
		$(IMAGE)

# Monte Carlo Golang
## Using the Monte Carlo algorithm all in Golang in the browser with the help of Web Assembly.

## To build:
```bash
./build.sh 

#or
GOOS=js GOARCH=wasm go build -o ./app/main.wasm cmd/webassembly/main.go
```

## To test:
```bash
go run cmd/server/main.go
# Server runs on http://localhost:9999

make run-docker
# Server runs on http://localhost:8888
```

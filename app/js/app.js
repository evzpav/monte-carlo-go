import { MonteCarloJs } from "./monteCarlo.js";
import { exampleTrades } from "./exampleTrades.js";

const calculateBtnEl = document.getElementById("calculate-btn");
const startEquityEl = document.getElementById("startEquity");
const marginEl = document.getElementById("margin");
const tradesPerYearEl = document.getElementById("tradesPerYear");
const nrIterationsEl = document.getElementById("nrIterations");
const resultsTableBodyEl = document.querySelector("#resultsTable > tbody");
const chartContainerEl = document.getElementById("chart-container");
const tradesEl = document.getElementById("trades");
const clearTradesBtnEl = document.getElementById("clear-trades-btn");
const lotSizeEl = document.getElementById("lotSize");
const chartDescriptionEl = document.getElementById("chart-description");
const errorsEl = document.getElementById("errors");
const loadingEl = document.getElementById("loading");

startEquityEl.value = 5000;
marginEl.value = 3000;
nrIterationsEl.value = 2500;
tradesPerYearEl.value = 151;
lotSizeEl.value = 1;
tradesEl.value = exampleTrades;

calculateBtnEl.disabled = true;
calculateBtnEl.addEventListener("click", calculate);

clearTradesBtnEl.addEventListener("click", function () {
    tradesEl.value = "";
})

window.onload = () => {
    Array.from(document.getElementsByTagName("input")).forEach((e) => {
        e.addEventListener("input", clearErrors)
    })
}

tradesEl.addEventListener("input", clearErrors)

// This is a polyfill for FireFox and Safari
// if (!WebAssembly.instantiateStreaming) {
//     WebAssembly.instantiateStreaming = async (resp, importObject) => {
//         const source = await (await resp).arrayBuffer()
//         return await WebAssembly.instantiate(source, importObject)
//     }
// }

// Promise to load the wasm file
function loadWasm(path) {
    const go = new Go()

    return new Promise((resolve, reject) => {
        WebAssembly.instantiateStreaming(fetch(path), go.importObject)
            .then(result => {
                go.run(result.instance)
                resolve(result.instance)
            })
            .catch(error => {
                reject(error)
            })
    })
}

// Load the wasm file
loadWasm("./main.wasm").then(wasm => {
    console.log("main.wasm is loaded 👋")
    calculateBtnEl.disabled = false;
    loadingEl.style.display = "none";
}).catch(error => {
    console.log("ouch", error)
    calculateBtnEl.disabled = false;
    loadingEl.style.display = "none";
})


function calculate() {
    const errorMsg = validateInput()
    if (errorMsg != "") {
        errorsEl.innerText = errorMsg;
        return
    }

    calculateBtnEl.setAttribute("aria-busy", true)

    const langRadioEl = document.querySelector('input[name="calcLanguage"]:checked');
    const calcLanguage = langRadioEl ? langRadioEl.value : "golang";
    console.log("Calculating Monte Carlo simulation with:", calcLanguage);

    const mc = {
        nrIterations: parseInt(nrIterationsEl.value),
        startEquity: parseFloat(startEquityEl.value),
        margin: parseFloat(marginEl.value),
        tradesPerYear: parseInt(tradesPerYearEl.value),
        lotSize: parseInt(lotSizeEl.value),
        trades: parseTrades(tradesEl.value)
    }

    const simulationResults = calcLanguage === "golang" ? MonteCarloGo(mc) : MonteCarloJs(mc);

    if (!simulationResults || simulationResults.length === 0) {
        // alert("Could not calculate. Please check your inputs")
        errorsEl.innerText = "Could not calculate. Please check your inputs"
        calculateBtnEl.setAttribute("aria-busy", false)
        return;
    }

    while (resultsTableBodyEl.hasChildNodes()) {
        resultsTableBodyEl.removeChild(resultsTableBodyEl.lastChild);
    }

    for (let i = 0; i < simulationResults.length; i++) {
        const sr = simulationResults[i]
        const row = resultsTableBodyEl.insertRow(i);
        row.setAttribute("id", "row_" + i)

        if (highlightRow(sr)) {
            row.setAttribute("class", "highlight")
        }

        row.insertCell().innerHTML = sr.startEquity
        row.insertCell().innerHTML = percentage(sr.return)
        row.insertCell().innerHTML = percentage(sr.drawdown)
        row.insertCell().innerHTML = round(sr.returnPerDrawdown, 1)
        row.insertCell().innerHTML = percentage(sr.totalRiskOfRuin)
        row.insertCell().innerHTML = percentage(sr.probProfitable)
    }

    while (chartContainerEl.hasChildNodes()) {
        chartContainerEl.removeChild(chartContainerEl.lastChild);
    }

    for (let i = 0; i < simulationResults.length; i++) {
        const sr = simulationResults[i];
        const articleEl = document.createElement("article")
        const canvasEl = document.createElement("canvas")
        canvasEl.setAttribute("id", "line-chart-" + sr.startEquity)

        chartContainerEl.appendChild(articleEl).appendChild(canvasEl)
        const ctx = canvasEl.getContext("2d")
        lineChart(sr, ctx)

    }

    calculateBtnEl.setAttribute("aria-busy", false)

    chartDescriptionEl.style.display = "block";
}

function highlightRow(sr) {
    return sr.totalRiskOfRuin <= 0.1 && sr.returnPerDrawdown >= 2;
}

function lineChart(simResults, chartCtx) {
    const equityCurves = simResults.minAndMaxEquityCurves;
    // const equityCurves = simResults.equityCurves;
    const numberOfItemsInArray = simResults.tradesPerCurve > 0 ? simResults.tradesPerCurve : 0

    const defaultColors = ["black"];

    let labels = []
    let marginLine = []
    for (let i = 1; i <= numberOfItemsInArray; i++) {
        labels.push(i)
        marginLine.push(parseFloat(simResults.margin))
    }

    const datasets = equityCurves.map((d, k) => {
        if ((k + 1) > defaultColors.length) {
            defaultColors.push(getColor())
        }

        return {
            data: d,
            label: k === 0 ? "original" : k + "",
            borderColor: defaultColors[k],
            backgroundColor: defaultColors[k],
            fill: false,
            pointRadius: 0,
            normalized: true
        }
    });

    datasets.push({
        data: marginLine,
        label: "margin",
        borderColor: "dark-grey",
        backgroundColor: "dark-grey",
        fill: false,
        borderDash: [5, 5],
        pointRadius: 0
    })

    new Chart(chartCtx, {
        type: "line",
        data: {
            labels,
            datasets
        },
        options: {
            spanGaps: true,
            animations: false,
            responsive: true,
            interaction: {
                mode: 'index',
                intersect: false,
            },
            plugins: {
                title: {
                    position: 'top',
                    align: 'start',
                    display: true,
                    padding: 20,
                    font: { size: 18 },
                    text: "Start equity " + simResults.startEquity + " USD",
                },
                legend: {
                    display: true,
                    position: 'bottom',
                },
                decimation: {
                    enabled: true,
                    algorithm: "lttb"
                }
            },
            scales: {
                x: {
                    type: 'linear',
                    min: 1,
                    max: parseInt(simResults.tradesPerCurve)
                },
                y: {
                    type: 'linear',
                    min: 0,
                    max: parseFloat(simResults.maxCurveValue)
                }
            }

        }
    });
}

function parseTrades(tradesInput) {
    const trades = [];
    tradesInput.split("\n").forEach(t => {
        if (t && t.length > 0) {
            if (!isNaN(t)) {
                trades.push(parseFloat(t))
            }
        }
    })

    return trades
}

function getColor() {
    const hueMin = 180
    const hueMax = 250
    const random = Math.random() * (hueMax - hueMin) + hueMin;
    return "hsl(" + random + ',' +
        (25 + 65 * Math.random()) + '%,' +
        (85 + 10 * Math.random()) + '%)'
}


function percentage(val, decimal = 0) {
    return round(val * 100, decimal) + "%"
}

function round(n, d = 2) {
    return Math.round(n * Math.pow(10, d)) / Math.pow(10, d)
}

function validateInput() {
    let errorMsg = "";

    const startEquity = startEquityEl.value;
    const margin = marginEl.value;
    const tradesPerYear = tradesPerYearEl.value;
    const nrIterations = nrIterationsEl.value;
    const lotSize = lotSizeEl.value;
    const trades = tradesEl.value;

    if (!startEquity || isNaN(startEquity) || parseFloat(startEquity) === 0) {
        errorMsg += "start equity is invalid\n"
    }

    if (!margin || isNaN(margin) || parseFloat(margin) > parseFloat(startEquity)) {
        errorMsg = errorMsg + "margin is invalid\n"
    }
    if (!tradesPerYear || isNaN(tradesPerYear) || parseInt(tradesPerYear) === 0) {
        errorMsg += "trades per year is invalid\n"
    }

    if (!nrIterations || isNaN(nrIterations) || parseInt(nrIterations) === 0) {
        errorMsg += "number of iterations is invalid\n"
    }

    if (!lotSize || isNaN(lotSize) || parseInt(lotSize) === 0) {
        errorMsg += "lot size is invalid\n"
    }

    if (!validateTrades(trades)) {
        errorMsg += "trades are invalid\n"
    }

    console.log(errorMsg)

    return errorMsg;
}


function validateTrades(tradesInput) {
    if (!tradesInput || tradesInput.length === 0) {
        return false;
    }

    const trades = tradesInput.split("\n")
    for (let i = 0; i < trades.length; i++) {
        const t = trades[i]
        if (t && t.length > 0 && isNaN(t)) {
            return false
        }
    }

    return true
}

function clearErrors() {
    errorsEl.innerText = "";
    calculateBtnEl.setAttribute("aria-busy", false);
}


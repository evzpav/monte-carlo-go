export function MonteCarloJs(mcInput) {
    const fixedFractional = mcInput.fixedFractional ? parseInt(mcInput.fixedFractional) : 0;
    const nrIterations = mcInput.nrIterations ? parseInt(mcInput.nrIterations) : 1;
    const startEquity = mcInput.startEquity ? parseFloat(mcInput.startEquity) : 1;
    const margin = parseFloat(mcInput.margin);
    const tradesPerYear = parseInt(mcInput.tradesPerYear);
    const lotSize = mcInput.lotSize ? parseInt(mcInput.lotSize) : 1;
    const trades = mcInput.trades;

    if (!areTradesValid(trades)) {
        return [];
    }

    const results = []

    let biggestLoss = 0
    for (let t of trades) {
        if (t < biggestLoss) {
            biggestLoss = t
        }
    }
    const equityIncrement = startEquity / 4;

    const iterator = tradesPerYear < trades.length ? tradesPerYear : trades.length

    for (let k = startEquity; k < startEquity * 3; k = k + equityIncrement) {
        const equities = []
        const drawdowns = []
        const ratesOfReturn = []
        const returnsPerDrawdown = []
        const profities = []
        let nrProfitable = 0;
        let nrRuin = 0;
        const equityCurves = {}

        const maxEquityCurves = {}
        const minEquityCurves = {}
        for (let i = 0; i < nrIterations; i++) { // nr of lines
            let ruin = false

            let equity = k
            let maxEquity = k
            let minEquity = 2 * k
            let drawdown = 0

            for (let j = 0; j < iterator; j++) {
                let t = 0;
                if (i === 0) {
                    t = trades[j]
                } else {
                    const randomIndex = Math.floor(Math.random() * trades.length)
                    t = trades[randomIndex]
                }

                let nrContracts = lotSize

                if (fixedFractional > 0) {
                    nrContracts = fixedFractional * equity / (-biggestLoss)
                }

                if (equity < margin) {
                    ruin = true
                    nrContracts = 0
                }

                equity += nrContracts * t

                if (equity > maxEquity) {
                    maxEquity = equity
                } else {
                    const dd = 1 - (equity / maxEquity)
                    if (dd > drawdown) {
                        drawdown = dd
                    }

                }

                if (equity < minEquity) {
                    minEquity = equity
                }


                if (maxEquityCurves[j] === undefined) {
                    maxEquityCurves[j] = { "value": 0, "curveId": null }
                } else if (maxEquityCurves[j].value < maxEquity) {
                    maxEquityCurves[j].value = maxEquity
                    maxEquityCurves[j].curveId = i
                }


                if (minEquityCurves[j] === undefined) {
                    minEquityCurves[j] = { "value": minEquity, "curveId": null }
                } else if (minEquityCurves[j].value > minEquity) {
                    minEquityCurves[j].value = minEquity
                    minEquityCurves[j].curveId = i
                }

                if (equityCurves[i] === undefined) {
                    equityCurves[i] = [k]
                }
                let arr = equityCurves[i]
                arr.push(equity)

            }

            if (ruin) {
                nrRuin++
            }

            const rateOfReturn = (equity / k) - 1
            const returnPerDrawdown = drawdown > 0 ? rateOfReturn / drawdown : 0
            const equityMinusInitial = equity - k

            if (equityMinusInitial > 0) {
                nrProfitable++
            }

            equities.push(equity)
            profities.push(equityMinusInitial)
            drawdowns.push(drawdown)
            ratesOfReturn.push(rateOfReturn)
            returnsPerDrawdown.push(returnPerDrawdown)
        }

        let minAndMaxCurvesIds = {}
        Object.values(maxEquityCurves).forEach((o) => {
            if (o.curveId) {
                minAndMaxCurvesIds[o.curveId] = true
            }
        })

        Object.values(minEquityCurves).forEach((o) => {
            if (o.curveId) {
                minAndMaxCurvesIds[o.curveId] = true
            }
        })

        const minAndMaxEquityCurves = [equityCurves[0]]
        Object.keys(equityCurves).forEach((id) => {
            if (minAndMaxCurvesIds[id] !== undefined) {
                minAndMaxEquityCurves.push(equityCurves[id])
            }
        })

        const simulationRes = {
            margin,
            startEquity: k,
            profit: median(profities),
            equity: median(equities),
            equityStd: round(standardDeviation(equities), 0),
            drawdown: median(drawdowns),
            drawdownStd: round(standardDeviation(drawdowns), 2),
            return: median(ratesOfReturn),
            returnStd: round(standardDeviation(ratesOfReturn), 2),
            returnPerDrawdown: median(returnsPerDrawdown),
            returnPerDrawdownStd: round(standardDeviation(returnsPerDrawdown), 2),
            probProfitable: nrProfitable / nrIterations,
            totalRiskOfRuin: nrRuin / nrIterations,
            minAndMaxEquityCurves,
        }

        results.push(simulationRes)
    }

    return results
}


// function shuffle(array) {
//     var i = array.length,
//         j = 0,
//         temp;

//     while (i--) {

//         j = Math.floor(Math.random() * (i + 1));

//         // swap randomly chosen element with current element
//         temp = array[i];
//         array[i] = array[j];
//         array[j] = temp;

//     }

//     return array;
// }

function median(arr) {
    const m = quickselect_median(arr)
    return round(m, 2)
}

function quickselect_median(arr) {
    const L = arr.length, halfL = L / 2;
    if (L % 2 === 1)
        return quickselect(arr, halfL);
    else
        return 0.5 * (quickselect(arr, halfL - 1) + quickselect(arr, halfL));
}

function quickselect(arr, k) {
    // Select the kth element in arr
    // arr: List of numerics
    // k: Index
    // return: The kth element (in numerical order) of arr
    if (arr.length === 1)
        return arr[0];
    else {
        const pivot = arr[0];
        const lows = arr.filter((e) => (e < pivot));
        const highs = arr.filter((e) => (e > pivot));
        const pivots = arr.filter((e) => (e === pivot));
        if (k < lows.length) // the pivot is too high
            return quickselect(lows, k);
        else if (k < lows.length + pivots.length)// We got lucky and guessed the median
            return pivot;
        else // the pivot is too low
            return quickselect(highs, k - lows.length - pivots.length);
    }
}

function round(n, d) {
    return Math.round(n * Math.pow(10, d)) / Math.pow(10, d)
}

function areTradesValid(trades) {
    if (!trades || trades.length === 0) {
        return false
    }

    for (let t of trades) {
        if (isNaN(t)) {
            return false
        }
    }

    return true
}


function standardDeviation(array) {
    const n = array.length
    const mean = array.reduce((a, b) => a + b) / n
    return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n)
}



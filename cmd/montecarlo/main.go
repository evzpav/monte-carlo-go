package main

import (
	"bufio"
	"fmt"
	"gitlab.com/evzpav/monte-carlo-go/montecarlo"
	"os"
	"strconv"
)

func main() {
	filePath := "mc-trades.txt"
	f, err := os.Open(filePath)
	if err != nil {
		fmt.Printf("Unable to read input file %v: %v\n", filePath, err)
		os.Exit(1)
	}
	defer f.Close()

	var trades []float64
	scanner := bufio.NewScanner(f)
	for scanner.Scan() { // internally, it advances token based on sperator
		if scanner.Text() != "" {
			t, err := strconv.ParseFloat(scanner.Text(), 64)
			if err != nil {
				fmt.Printf("failed to parse:%v", err)
				os.Exit(1)
			}

			trades = append(trades, t)
		}
	}

	input := montecarlo.MonteCarloInput{
		NrIterations:  5000,
		StartEquity:   5000,
		Margin:        3000,
		TradesPerYear: 151,
		LotSize:       1,
		Trades:        trades,
	}
	res, err := montecarlo.Run(input)
	if err != nil {
		panic(err)
	}

	for _, _ = range res {
		// fmt.Printf("%+v\n", r)
		// fmt.Printf("MinAndMaxEquityCurves len: %d\n", len(r.MinAndMaxEquityCurves))
		// fmt.Printf("First curve:%d\n", len(r.MinAndMaxEquityCurves[0]))
		// fmt.Printf("MINMAX CURVES:%+v\n", r.MinAndMaxEquityCurves)
	}

}

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	port := os.Getenv("PORT")
	if port == "" {
		port = "9999"
	}
	fmt.Print("Server running on http://localhost:" + port)
	err := http.ListenAndServe(":"+port, http.FileServer(http.Dir("./app")))
	if err != nil {
		log.Fatalf("Failed to start server %v", err)
	}

}

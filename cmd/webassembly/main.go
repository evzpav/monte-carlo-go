package main

import (
	"encoding/json"
	"fmt"
	"log"
	"syscall/js"

	"gitlab.com/evzpav/monte-carlo-go/montecarlo"
)

func MonteCarlo(this js.Value, args []js.Value) interface{} {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from", r)
		}
	}()

	if len(args) < 1 {
		return 0
	}

	monteCarloInput := args[0]
	nrIterations := monteCarloInput.Get("nrIterations").Int()
	startEquity := monteCarloInput.Get("startEquity").Float()
	margin := monteCarloInput.Get("margin").Float()
	tradesPerYear := monteCarloInput.Get("tradesPerYear").Int()
	lotSize := monteCarloInput.Get("lotSize").Int()
	tradesVal := monteCarloInput.Get("trades")

	var trades []float64
	for i := 0; i < tradesVal.Length(); i++ {
		t := tradesVal.Index(i).Float()
		trades = append(trades, t)
	}

	withEquityCurves := true

	input := montecarlo.MonteCarloInput{
		NrIterations:     nrIterations,
		StartEquity:      startEquity,
		Margin:           margin,
		TradesPerYear:    tradesPerYear,
		LotSize:          lotSize,
		Trades:           trades,
		WithEquityCurves: &withEquityCurves,
	}
	res, err := montecarlo.Run(input)
	if err != nil {
		log.Fatal(err)
	}

	// for _, r := range res {
	// 	fmt.Printf("%+v\n", r)
	// }

	bs, err := json.Marshal(res)
	if err != nil {
		return err
	}

	var result []interface{}
	if err := json.Unmarshal(bs, &result); err != nil {
		return err
	}

	return result
}

func main() {
	fmt.Println("🇲🇨 Monte Carlo Simulation in Golang via WebAssembly")

	js.Global().Set("MonteCarloGo", js.FuncOf(MonteCarlo))
	<-make(chan bool)
}

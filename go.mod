module gitlab.com/evzpav/monte-carlo-go

go 1.15

require (
	github.com/jedib0t/go-pretty/v6 v6.3.5
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/testify v1.7.4
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

package montecarlo

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"sort"
	"time"

	"github.com/jedib0t/go-pretty/v6/table"
)

type MonteCarloInput struct {
	NrIterations     int
	StartEquity      float64
	Margin           float64
	TradesPerYear    int
	LotSize          int
	Trades           []float64
	FixedFractional  float64
	WithEquityCurves *bool
}

func (mci MonteCarloInput) validate() error {
	if mci.NrIterations <= 0 {
		return fmt.Errorf("nrIterations is invalid")
	}

	if mci.NrIterations > 20000 {
		return fmt.Errorf("too many iterations. Try < 20000")
	}

	if mci.StartEquity <= 0 {
		return fmt.Errorf("startEquity is invalid")
	}

	if mci.Margin <= 0 {
		return fmt.Errorf("margin is invalid")
	}

	if mci.StartEquity < mci.Margin {
		return fmt.Errorf("startEquity needs to be higher than margin")
	}

	if mci.LotSize <= 0 {
		return fmt.Errorf("lotSize is invalid")
	}

	if mci.TradesPerYear <= 0 {
		return fmt.Errorf("tradesPerYear is invalid")
	}

	if len(mci.Trades) == 0 {
		return fmt.Errorf("trades are required")
	}

	if mci.WithEquityCurves == nil {
		return fmt.Errorf("WithEquityCurves required")
	}

	return nil
}

type SimulationResults []SimulationResult
type SimulationResult struct {
	Margin                float64     `json:"margin"`
	StartEquity           float64     `json:"startEquity"`
	TradesPerCurve        int         `json:"tradesPerCurve"`
	MaxCurveValue         float64     `json:"maxCurveValue"`
	Profit                float64     `json:"profit"`
	Equity                float64     `json:"equity"`
	Drawdown              float64     `json:"drawdown"`
	Return                float64     `json:"return"`
	ReturnPerDrawdown     float64     `json:"returnPerDrawdown"`
	ProbProfitable        float64     `json:"probProfitable"`
	TotalRiskOfRuin       float64     `json:"totalRiskOfRuin"`
	MinAndMaxEquityCurves [][]float64 `json:"minAndMaxEquityCurves"`
	EquityCurves          [][]float64 `json:"equityCurves"`
	// AvgEquityCurve        []float64   `json:"avgEquityCurve"`
	// MedianEquityCurve     []float64   `json:"medianEquityCurve"`
	OriginalEquityCurve []float64 `json:"originalEquityCurve"`
}

type Curve struct {
	Value       float64
	TradeNumber int
	ValueSet    bool
}

func Run(mci MonteCarloInput) (SimulationResults, error) {
	defer measureTime("MonteCarloSimulationGo")()

	if err := mci.validate(); err != nil {
		return nil, err
	}
	maxNumberOfCurves := 30
	fixedFractional := mci.FixedFractional
	startEquity := mci.StartEquity
	nrIterations := mci.NrIterations
	tradesPerYear := mci.TradesPerYear
	lotSize := mci.LotSize
	margin := mci.Margin
	trades := mci.Trades

	equityIncrement := startEquity / 4

	var results SimulationResults

	var biggestLoss float64
	for _, t := range trades {
		if t < biggestLoss {
			biggestLoss = t
		}
	}

	tradesPerCurve := len(trades)
	if tradesPerYear < len(trades) {
		tradesPerCurve = tradesPerYear
	}

	for k := startEquity; k <= startEquity*2; k = k + equityIncrement {
		var equities, drawdowns, ratesOfReturn, returnsPerDrawdown, profities []float64
		nrProfitable := 0
		nrRuin := 0
		tradeNrToEquityCurves := make(map[int][]float64)
		maxEquityCurves := make(map[int]Curve)
		minEquityCurves := make(map[int]Curve)

		for i := 0; i < nrIterations; i++ {
			time.Sleep(time.Nanosecond * 2)
			rand.Seed(time.Now().UnixNano())

			var ruin bool

			equity := k
			maxEquity := k
			minEquity := 2 * k
			var drawdown float64

			for tradeNumber := 0; tradeNumber < tradesPerCurve; tradeNumber++ {
				var t float64
				if i == 0 {
					t = trades[tradeNumber]
				} else {
					randomIndex := rand.Intn(len(trades))
					t = trades[randomIndex]
				}

				nrContracts := lotSize

				if fixedFractional > 0 {
					nrContracts = int(fixedFractional * equity / (-biggestLoss))
				}

				if equity < margin {
					ruin = true
					nrContracts = 0
				}

				equity += float64(nrContracts) * t

				if equity > maxEquity {
					maxEquity = equity
				} else {
					dd := 1 - (equity / maxEquity)
					if dd > drawdown {
						drawdown = dd
					}

				}

				if equity < minEquity {
					minEquity = equity
				}

				if *mci.WithEquityCurves {
					// max equity curves
					maxCurve, ok := maxEquityCurves[tradeNumber]
					if ok {
						if maxCurve.Value < maxEquity {
							maxCurve.Value = maxEquity
							maxCurve.TradeNumber = i
							maxCurve.ValueSet = true
							maxEquityCurves[tradeNumber] = maxCurve
						}
					} else {
						maxEquityCurves[tradeNumber] = Curve{}
					}

					// min equity curves
					minCurve, ok := minEquityCurves[tradeNumber]
					if ok {
						if minCurve.Value > minEquity {
							minCurve.Value = minEquity
							minCurve.TradeNumber = i
							minCurve.ValueSet = true
							minEquityCurves[tradeNumber] = minCurve
						}
					} else {
						minEquityCurves[tradeNumber] = Curve{Value: minEquity}
					}

					_, ok = tradeNrToEquityCurves[i]
					if !ok {
						tradeNrToEquityCurves[i] = []float64{k}
					}

					tradeNrToEquityCurves[i] = append(tradeNrToEquityCurves[i], equity)
				}
			}

			if ruin {
				nrRuin++
			}

			rateOfReturn := (equity / k) - 1

			var returnPerDrawdown float64

			if drawdown > 0 {
				returnPerDrawdown = rateOfReturn / drawdown
			}

			equityMinusInitial := equity - k

			if equityMinusInitial > 0 {
				nrProfitable++
			}

			equities = append(equities, equity)
			profities = append(profities, equityMinusInitial)
			drawdowns = append(drawdowns, drawdown)
			ratesOfReturn = append(ratesOfReturn, rateOfReturn)
			returnsPerDrawdown = append(returnsPerDrawdown, returnPerDrawdown)
		}

		minAndMaxCurvesTradeNrs := make(map[int]bool)

		var maxCurveValue float64
		for _, c := range maxEquityCurves {
			if c.ValueSet {
				minAndMaxCurvesTradeNrs[c.TradeNumber] = true
				if c.Value > maxCurveValue {
					maxCurveValue = c.Value
				}
			}
		}
		for _, c := range minEquityCurves {
			if c.ValueSet {
				minAndMaxCurvesTradeNrs[c.TradeNumber] = true
			}

		}

		var minAndMaxEquityCurves [][]float64
		var equityCurves [][]float64

		originalEquityCurve := tradeNrToEquityCurves[0]

		if *mci.WithEquityCurves {
			minAndMaxEquityCurves = append(minAndMaxEquityCurves, originalEquityCurve)

			for tradeNumber, ec := range tradeNrToEquityCurves {
				_, ok := minAndMaxCurvesTradeNrs[tradeNumber]
				if ok && len(minAndMaxEquityCurves) <= maxNumberOfCurves {
					minAndMaxEquityCurves = append(minAndMaxEquityCurves, ec)
				}

				if tradeNumber <= maxNumberOfCurves {
					equityCurves = append(equityCurves, ec)
				}
			}
		}

		simulationRes := SimulationResult{
			Equity:                Median(equities),
			Profit:                Median(profities),
			Drawdown:              Median(drawdowns),
			ReturnPerDrawdown:     Median(returnsPerDrawdown),
			Return:                Median(ratesOfReturn),
			ProbProfitable:        float64(nrProfitable) / float64(nrIterations),
			TotalRiskOfRuin:       float64(nrRuin) / float64(nrIterations),
			Margin:                margin,
			StartEquity:           k,
			TradesPerCurve:        tradesPerCurve,
			MaxCurveValue:         maxCurveValue,
			MinAndMaxEquityCurves: minAndMaxEquityCurves,
			EquityCurves:          equityCurves,
		}

		results = append(results, simulationRes)
	}


	return results, nil

}

func (sr SimulationResults) PrintTableToTerminal() {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)

	t.AppendHeader(table.Row{
		"StartEquity",
		"Profit",
		"Equity",
		"Drawdown",
		"Return",
		"ReturnPerDrawdown",
		"ProbProfitable",
		"TotalRiskOfRuin",
	})

	for _, r := range sr {
		t.AppendRows([]table.Row{
			{
				r.StartEquity,
				r.Profit,
				r.Equity,
				r.Drawdown,
				r.Return,
				r.ReturnPerDrawdown,
				r.ProbProfitable,
				r.TotalRiskOfRuin,
			},
		})
	}

	t.Render()
}

func Remove(s []int, i int) []int {
	if i != len(s)-1 {
		s[i] = s[len(s)-1]
	}

	return s[:len(s)-1]
}

func Median(array []float64) float64 {
	m := median(array)
	return roundTo(m, 4)
}

func Average(array []float64) float64 {
	var sum float64
	for _, a := range array {
		sum += a
	}
	avg := sum / float64(len(array))
	return roundTo(avg, 4)
}

func median(array []float64) float64 {
	sort.Float64s(array) // sort the numbers

	mNumber := len(array) / 2

	if len(array)%2 != 0 {
		return array[mNumber]
	}

	return (array[mNumber-1] + array[mNumber]) / 2
}

func roundTo(n float64, decimals uint32) float64 {
	return math.Round(n*math.Pow(10, float64(decimals))) / math.Pow(10, float64(decimals))
}

func measureTime(funcName string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("Time taken by %s function is %v \n", funcName, time.Since(start))
	}
}

// func stdDeviation(array []float64) float64 {
// 	variance := stat.Variance(array, nil)
// 	return math.Sqrt(variance)
// }

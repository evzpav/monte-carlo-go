package montecarlo_test

import (
	"testing"

	"gitlab.com/evzpav/monte-carlo-go/montecarlo"

	"github.com/stretchr/testify/assert"
)

func TestSimulation(t *testing.T) {
	trades := []float64{100.0, -50.0, 150.0, -10.0, 25.0, 60.0, -40.0}

	t.Run("with equity curves", func(t *testing.T) {
		withEquityCurves := true
		mci := montecarlo.MonteCarloInput{
			NrIterations:     10,
			StartEquity:      4000,
			Margin:           3000,
			TradesPerYear:    100,
			Trades:           trades,
			LotSize:          1,
			WithEquityCurves: &withEquityCurves,
		}

		res, err := montecarlo.Run(mci)
		assert.Nil(t, err)
		assert.GreaterOrEqual(t, len(res[0].MinAndMaxEquityCurves), 1)

		res.PrintTableToTerminal()
	})

	t.Run("without equity curves", func(t *testing.T) {
		withEquityCurves := false
		mci := montecarlo.MonteCarloInput{
			NrIterations:     10,
			StartEquity:      4000,
			Margin:           3000,
			TradesPerYear:    100,
			Trades:           trades,
			LotSize:          1,
			WithEquityCurves: &withEquityCurves,
		}

		res, err := montecarlo.Run(mci)
		assert.Nil(t, err)
		assert.Len(t, res[0].MinAndMaxEquityCurves, 0)
	})

}
